package raul.programming.absolutejava;

public class Demo_DateStuff {

	public static void main(String[] args)
	{
		DateFirstTry date1, date2, date3;
		date1 = new DateFirstTry();
		date2 = new DateFirstTry();
		date3 = new DateFirstTry();
		
		date1.month = "October";
		date1.day = 29;
		date1.year = 2020;
		System.out.println("date1:");
		date1.makeItNewYears();
		date1.writeOutput();
		date1.yellIfNewYear();
		
		date2.month = "January";
		date2.day = 10;
		date2.year = 2020;
		System.out.println("date2:");
		date2.writeOutput();
		date2.yellIfNewYear();
		
	
		
		date3.month = "November";
		date3.day = 10;
		date3.year = 1999;
		System.out.println("data3:");
		date3.makeItNewYears();
		date3.writeOutput();
		date3.yellIfNewYear();
		
	}
}

