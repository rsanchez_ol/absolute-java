package raul.programming.absolutejava;

/**
 * Some new exercises from Chapter 4, see page 212 of the book.. "Self-Test Exercises"
 * 
 */

public class DateFirstTry
{
	public String month;
	public int day;
	public int year; // a four digit number
	
	public void writeOutput() 
	{
		System.out.println(month + " " + day + ", " + year);
	}
	
	public void makeItNewYears()
	{
		month = "January";
		day = 1;
		
	}
	
	public void yellIfNewYear()
	{
		if (month == "January" && day == 1)
				
			System.out.println("Hurrah!");
	
		else 
			System.out.println("Not New Year's Day");
		
	}
}

